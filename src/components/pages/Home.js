import React, { Component } from 'react';
import AddTodo from '../todo/AddTodo';
import Filter from '../Filter';
import Todos from '../todo/Todos';

class Home extends Component {
  state = {
    filter: ''
  }

  filterTodo = (content) => {
    this.setState({filter: content})
  }

  render() {
    return (
      <div className="pt-5">
        <div className="container">
        <Filter filterTodo={this.filterTodo}/>
          <Todos filterKeyword={this.state.filter} />
        </div>
          <AddTodo addTodo={this.addTodo} />
      </div>
    )
  }
}

export default Home;