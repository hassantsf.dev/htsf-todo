import React from 'react';

const About = () => {
  return(
    <div className="container bg-light mt-4 p-4">
      <h3 className="display-3">Todo App</h3>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel tenetur eius iste voluptatibus voluptate autem iure explicabo eum inventore nobis perferendis magni nihil dignissimos fugiat consequuntur voluptatem nam sunt, suscipit aliquid corporis. Dolores, aperiam ipsam doloremque quisquam excepturi quo corporis odit totam fugiat, similique ex quis impedit! Nihil molestiae voluptates eligendi sit accusamus debitis suscipit quae, fugiat enim tenetur assumenda fugit laboriosam quas est voluptatem. Quisquam ducimus quas culpa, nostrum totam, veniam, vel reiciendis dolor corrupti molestiae architecto obcaecati non! Ea accusantium magni consequuntur? Veniam molestiae ut eveniet mollitia ratione voluptatem omnis, amet dolores odio, corporis hic dolorum tenetur! Cumque.
      </p>
    </div>
  );
};

export default About;