const initState = {
  todos: [
    { title: 'Listen Music', id: 1, done: false, description: 'Listen to rock music on after noon', priorety: 'high' },
    { title: 'Watch Movie', id: 2, done: false, description: 'Watch Avengers Endgame with friends', priorety: 'medium' },
    { title: 'Learn Redux', id: 3, done: false, description: 'Write this program with redux', priorety: 'low' },
    { title: 'Drink Coffee', id: 4, done: false, decription: 'Go Park and Drink Coffee', priorety: null }
  ]
}

function rootReducer(state = initState, action) {
  switch (action.type) {
    case 'DELETE_TODO':
      let todos = state.todos.filter((todo) => (
        todo.id !== action.id
      ));

      todos = todos.map((todo) => {
        if (todo.id >= action.id) {
          todo.id--;
        }
        return todo;
      });
      return {
        ...state,
        todos
      }

    case 'MARK_TODO':
      return {
        ...state,
        todos: markTodo(state.todos, action.id)
      }

    case 'ADD_TODO':
      return {
        ...state,
        todos: addTodo(state.todos, action.title, action.priorety)
      }

    default:
      return state;
  }
}

function markTodo(todos, id) {
  const copyTodos = [...todos];
  for (const todo of copyTodos) {
    if (todo.id === id) {
      todo.done = !todo.done;
    }
  }
  return copyTodos;
}

function addTodo(todos, title, priorety) {
  console.log(priorety)
  const newTodo = {
    title,
    id: findMaxId(todos) + 1,
    done: false,
    date: new Date(),
    priorety
  };

  const copyTodos = [...todos, newTodo];
  return copyTodos;
}

function findMaxId(todos) {
  let lastTodo;
  try {
    lastTodo = todos.reduce((max, current) => (
      current.id > max.id ? current : max
    ));
  } catch (e) {
    return 0;
  }

  return lastTodo.id;
}

export default rootReducer