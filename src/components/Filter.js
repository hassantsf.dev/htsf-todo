import React, { Component } from 'react';

class Filter extends Component {
  state = {
    pharase: ''
  }

  handleChange = (e) => {
    this.props.filterTodo(e.target.value.toLowerCase());
  }

  render() {
    return (
      <div className="filter-todo mb-4">
        <input type="text" className="form-control" placeholder="Filter Todo..." onChange={this.handleChange} />
      </div>
    )
  }
}

export default Filter;