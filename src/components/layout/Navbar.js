import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="navbar navbar bg-dark navbar-dark navbar-expand py-3">
      <div className="container">
        <Link to="/" className="navbar-brand">HTSF Todo</Link>

        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink exact to="/" className="nav-link">Home</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/about" className="nav-link">About</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/contact" className="nav-link">Contact</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;