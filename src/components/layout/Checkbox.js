import React from 'react'
import { connect } from 'react-redux'
import { markTodo } from '../../actions/todoAction'
import './Checkbox.css'

function getStyle(priorety) {
  let color;
  switch(priorety) {
    case 'low':
      color = '#009BFF';
      break;
    case 'medium':
      color = 'yellow';
      break;
    case 'high':
      color = 'red';
      break;

    default:
      color = '#ccc';
  }

  return {
    borderColor: color
  }
}

function Checkbox({ id, priorety, markTodo }) {
  return (
    <div className="checkbox-wrapper">
      <div className="checkbox-container mr-2">
        <label className="checkbox-label">
          <input type="checkbox" onChange={(e) => { markTodo(id) }} />
          <span className="checkbox-custom rectangular" style={getStyle(priorety)}></span>
        </label>
      </div>
    </div>
  )
}


const mapDispatchToProps = (dispatch) => (
  {
    markTodo: (id) => { dispatch(markTodo(id)) }
  }
)

export default connect(null, mapDispatchToProps)(Checkbox);