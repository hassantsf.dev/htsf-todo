import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar'
import Home  from './components/pages/Home';
import Contact from './components/pages/Contact';
import About from './components/pages/About';
import DetailedTodo from './components/todo/DetailedTodo';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Navbar />
          <Route exact path='/' component={() => <Home getTodo={this.getTodo} />} />
          <Route path='/about' component={About} />
          <Route path='/contact' component={Contact} />
          <Route path="/todo/:todo_id" component={DetailedTodo}/>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;