import React, { Component } from 'react';
import './AddTodo.css';
import { addTodo } from '../../actions/todoAction'
import { connect } from 'react-redux'

class AddTodo extends Component {
  state = {
    title: '',
    priorety: ''
  }

  handleChange = (e) => {
    console.log(e.target.value)
    this.setState({ [e.target.name]: e.target.value });
  }

  addTodo = (e) => {
    const { title, priorety } = this.state;
    this.props.addTodo(title, priorety);
  }


  render() {
    return (
      <div className="mt-4">
        <div className="container">
          <button className="btn btn-success btn-block" data-target="#add-todo" data-toggle="modal">Add Todo</button>
        </div>

        <div id="add-todo" className="modal fade">
          <div className="modal-dialog">
            <div className="modal-content">
              <header className="modal-header">
                <h2 className="modal-title">Add Todo</h2>
                <button className="close nav-link" data-dismiss="modal">&times;</button>
              </header>

              <main className="modal-body">
                <form onSubmit={this.addTodo}>
                  <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input type="text" id="title" name="title" className="form-control" value={this.state.todo} onChange={this.handleChange} />
                  </div>
        
                  <div className="form-group">
                    <label htmlFor="priorety">Priorety</label>
                    <select name="priority" id="priority" className="form-control" onChange={this.handleChange}>
                      <option value="none">None</option>
                      <option value="high">High</option>
                      <option value="medium">Medium</option>
                      <option value="low">Low</option>
                    </select>
                  </div>
                </form>
              </main>

              <footer className="modal-footer">
                <button className="btn btn-success btn-block" onClick={this.addTodo} data-dismiss="modal">Add</button>
              </footer>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    addTodo: (title, priorety) => {dispatch(addTodo(title, priorety))}
  }
)

export default connect(null ,mapDispatchToProps)(AddTodo);