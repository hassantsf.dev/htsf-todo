import React, { Component } from 'react';
import {connect } from 'react-redux';
import { deleteTodo } from '../actions/todoAction';
import './Action.css';

class Action extends Component {
  state = {
    done: false
  }

  removeTodo = (id) => {
    this.props.deleteTodo(id)
  }

  render() {
    return (
      <div className="action">
        <button className="btn nav-link text-danger p-0" onClick={(e) => { this.removeTodo(this.props.id) }}>&times;</button>
      </div>
    )
  }
};

const mapDispatchToProps = (dispatch) => (
  {
    deleteTodo: (id) => { dispatch(deleteTodo(id)) }
  }
)

export default connect(null, mapDispatchToProps)(Action);