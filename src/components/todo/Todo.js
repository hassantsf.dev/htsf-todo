import React from 'react';
import Action from '../Action';
import Checkbox from '../layout/Checkbox';
import { Link } from 'react-router-dom';

const getMarkedTodoStyle = (done) => {
  return {
    textDecoration: done ? 'line-through' : 'none'
  }
}

const Todo = ({ todo }) => {
  return (
    <div className="list-group-item d-flex justify-content-between align-items-center">
      <div className="task d-flex justify-content-between">
        <Checkbox id={todo.id} priorety={todo.priorety} />
        <div className={todo.done ? 'deleted' : ''} style={getMarkedTodoStyle(todo.done)} >
          <Link to={'/todo/' + todo.id } className="text-dark nav-link">
          {todo.title}
          </Link>
          </div>
      </div>
      <Action id={todo.id} />
    </div>
  );
}

// Todo: Priorety doesn't work after add Todo doens't show default style
//Todo: Checkbox doesn't update the store

export default Todo;