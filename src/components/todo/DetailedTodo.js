import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

export class DetailedTodo extends Component {
  getCompletedStyle(done) {
    if (done) {
      return 'badge-success'
    } else {
      return 'badge-danger'
    }
  }
  getPrioretyStyle(priorety) {
    switch (priorety) {
      case 'high':
        return 'badge-danger'

      case 'medium':
        return 'badge-warning'

      case 'low':
        return 'badge-primary'

      default:
        return 'badge-secondary'
    }
  }

  render() {
    const { title, description, done, priorety } = this.props.todo;
    return (
      <div className="container py-5">
        <Link to="/" className="btn btn-outline-secondary mb-3">Back To Home</Link>
        <div className="d-flex justify-content-between align-items-center">
          <h1 className="mb-4">{title}</h1>
          <div>
            <div className={"status mr-3 py-2 px-3 badge " + this.getPrioretyStyle(priorety)}>{priorety}</div>
            <div className={"status py-2 px-3 badge " + this.getCompletedStyle(done)}>{done ? 'Done' : 'Undone'}</div>
          </div>
        </div>
        <p className="lead">Description: {description}</p>
      </div>
    )
  }
}



const mapStateToProps = (state, ownProps) => {
  const id = parseInt(ownProps.match.params.todo_id);

  return {
    todo: state.todos.find((todo) => todo.id === id)
  }
}

export default connect(mapStateToProps)(DetailedTodo);
