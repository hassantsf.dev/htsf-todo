import React from 'react';

const Contact = () => {
  return(
    <div className="container bg-light mt-4 p-4 text-dark">
      <h2>Contact Us</h2>
      <form>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input type="text" id="name" className="form-control"/>
        </div>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" className="form-control"/>
        </div>
        <div className="form-group">
          <label htmlFor="message">Message</label>
          <textarea name="message" id="message"  className="form-control"></textarea>
        </div>

        <button className="btn btn-info btn-block">Send</button>
      </form>
    </div>
  )
};

export default Contact;