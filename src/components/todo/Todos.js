import React from 'react';
import './Todos.css';
import Todo from './Todo';
import { connect } from 'react-redux';

const Todos = ({ todos,  filterKeyword }) => {
  let todosList;
  if (todos.length === 0) {
    todosList = <div className="alert alert-success text-center">
      congratulations! <strong>All titles</strong> are done.
    </div>
  } else {
    todosList = todos.map((todo) => {
      if (todo.title.toLowerCase().indexOf(filterKeyword) !== -1) {
        return <Todo todo={todo}  key={todo.id} />
      } else {
        return null;
      }
    });
  }

const itIsEmptyMessage = <div className="alert alert-info">There is no <strong>Todo</strong> wit{filterKeyword} keyword here!</div>;

  return (
    <div className="todos">
      <div className="list-group">
        {
        todosList.key === null ? todosList :
        todosList.every(todo => todo === null) ? (itIsEmptyMessage) : todosList}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => (
  {
    todos: state.todos
  }
)

export default connect(mapStateToProps)(Todos);