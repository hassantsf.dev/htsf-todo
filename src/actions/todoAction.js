export const deleteTodo = (id) => (
  {
    type: 'DELETE_TODO',
    id
  }
)

export const markTodo = (id) => (
  {
    type: 'MARK_TODO',
    id
  }
)

export const addTodo = (title, priorety) => (
  {
    type: 'ADD_TODO',
    title,
    priorety
  }
)